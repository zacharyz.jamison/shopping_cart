package com.onlinestore.OnlineStoreServer.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.onlinestore.OnlineStoreServer.entity.Users;
import com.onlinestore.OnlineStoreServer.repo.User_Repository;

@Service
public class UserService {

	@Autowired
	User_Repository userRepository;
	
	public List<Users> getAllUsers() {
		return userRepository.findAll();
	}

	public Users getUser(int user_id) {
		return userRepository.findById(user_id);
	}
	
	public Users checkUserExist(String username, String password) {
		return userRepository.findByUsernameAndPassword(username, password);
	}
	
	public String createUser(Users user) {
		userRepository.save(user);
		return "User Created";
	}
}

