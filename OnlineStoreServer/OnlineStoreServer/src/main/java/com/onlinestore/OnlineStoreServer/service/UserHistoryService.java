package com.onlinestore.OnlineStoreServer.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.onlinestore.OnlineStoreServer.entity.UserHistory;
import com.onlinestore.OnlineStoreServer.repo.UserHistoryRepository;

@Service
public class UserHistoryService {

	@Autowired
	UserHistoryRepository userHistoryRepository;
	
	public List<UserHistory> getHistory(String username) {
		return userHistoryRepository.findByUsername(username);
	}
	
	public String insertHistory(List<UserHistory> userHistory) {
		userHistoryRepository.saveAll(userHistory);
		return "Success";
	}
	
}
