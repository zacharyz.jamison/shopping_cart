package com.onlinestore.OnlineStoreServer.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.onlinestore.OnlineStoreServer.entity.BuyerHistory;
import com.onlinestore.OnlineStoreServer.repo.BuyerHistoryRepository;

@Service
public class BuyerHistoryService {
	
	@Autowired
	BuyerHistoryRepository buyerHistoryRepository;
	
	public List<BuyerHistory> getHistory(String username) {
		return buyerHistoryRepository.findByBuyerName(username);
	}
	
	public String insertHistory(BuyerHistory buyerHistory) {
		buyerHistoryRepository.save(buyerHistory);
		return "Success";
	}
}
