package com.onlinestore.OnlineStoreServer.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity(name="buyer_history")
public class BuyerHistory {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="history_id")
	private int id;
	@Column(name="buyer_name")
	private String buyerName;
	@Column(name="name")
	private String name;
	@Column(name="description")
	private String description;
	@Column(name="price")
	private int price;
	
	
	public BuyerHistory() {
		super();
	}

	public BuyerHistory(int id, String buyerName, String name, String description, int price) {
		super();
		this.id = id;
		this.buyerName = buyerName;
		this.name = name;
		this.description = description;
		this.price = price;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getBuyerName() {
		return buyerName;
	}

	public void setBuyerName(String buyerName) {
		this.buyerName = buyerName;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	@Override
	public String toString() {
		return "BuyerHistory [id=" + id + ", buyerName=" + buyerName + ", name=" + name + ", description=" + description
				+ ", price=" + price + "]";
	}

	

	
}
