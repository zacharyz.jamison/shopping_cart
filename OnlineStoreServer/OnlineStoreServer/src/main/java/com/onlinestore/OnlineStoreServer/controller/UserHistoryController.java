package com.onlinestore.OnlineStoreServer.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.onlinestore.OnlineStoreServer.entity.UserHistory;
import com.onlinestore.OnlineStoreServer.service.UserHistoryService;

@RestController
@RequestMapping("/userhistory")
public class UserHistoryController {
	
	@Autowired
	UserHistoryService userHistoryService;

	@GetMapping("/{username}")
	public ResponseEntity<?> getUserHistory(@PathVariable("username") String username) {
		List<UserHistory> userHistory = userHistoryService.getHistory(username);
		if(!userHistory.isEmpty()) {
			return ResponseEntity.ok(userHistory);
		} else {
			return ResponseEntity.ok(new int[0]);
		}
	}
	
	@PostMapping()
	public ResponseEntity<?> insertUserHistory(@RequestBody List<UserHistory> userlist) {
		return ResponseEntity.ok(userHistoryService.insertHistory(userlist));
	}

}
