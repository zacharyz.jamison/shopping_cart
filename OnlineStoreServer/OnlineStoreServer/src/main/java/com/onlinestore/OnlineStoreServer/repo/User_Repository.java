package com.onlinestore.OnlineStoreServer.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.onlinestore.OnlineStoreServer.entity.Users;

public interface User_Repository extends JpaRepository<Users, Integer>{

	public Users findById(int user_id);
	public Users findByUsernameAndPassword(String username, String password);
	
}
