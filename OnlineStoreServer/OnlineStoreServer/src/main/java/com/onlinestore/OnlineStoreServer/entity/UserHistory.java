package com.onlinestore.OnlineStoreServer.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity(name="user_history")
public class UserHistory {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="history_id")
	private int id;
	@Column(name="username")
	private String username;
	@Column(name="name")
	private String name;
	@Column(name="description")
	private String description;
	@Column(name="price")
	private int price;
	@Column(name="img")
	private String img;
	
	public UserHistory() {
		super();
	}

	public UserHistory(int id, String username, String name, String description, int price, String img) {
		super();
		this.id = id;
		this.username = username;
		this.name = name;
		this.description = description;
		this.price = price;
		this.img = img;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	public String getImg() {
		return img;
	}

	public void setImg(String img) {
		this.img = img;
	}

	@Override
	public String toString() {
		return "UserHistory [id=" + id + ", username=" + username + ", name=" + name + ", description=" + description
				+ ", price=" + price + ", img=" + img + "]";
	}

	
}
