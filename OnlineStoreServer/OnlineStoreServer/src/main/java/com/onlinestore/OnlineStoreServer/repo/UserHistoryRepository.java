package com.onlinestore.OnlineStoreServer.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.onlinestore.OnlineStoreServer.entity.UserHistory;

public interface UserHistoryRepository extends JpaRepository<UserHistory, Integer> {
	public List<UserHistory> findByUsername(String username);
}
