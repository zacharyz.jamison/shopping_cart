package com.onlinestore.OnlineStoreServer.controller;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.onlinestore.OnlineStoreServer.entity.Users;
import com.onlinestore.OnlineStoreServer.service.UserService;

@RestController
@RequestMapping("/users")
public class UserController {
	
	@Autowired
	UserService userService;

	@GetMapping
	public ResponseEntity<List<Users>> getUsers() {
		return ResponseEntity.ok(userService.getAllUsers());
	}
	
	@GetMapping("/{user_id}")
	public ResponseEntity<Users> getUser(@PathVariable("user_id") int user_id) {
		return ResponseEntity.ok(userService.getUser(user_id));
	}
	
	@GetMapping("/userCheck")
	public ResponseEntity<Boolean> checkUser(HttpSession session) {
		//HttpSession check = session.getAttribute("user");
		if(session.getAttribute("user") != null) {
			return ResponseEntity.ok(true);
		} else {
			return ResponseEntity.ok(false);
		}
	}
	
	@PostMapping
	public ResponseEntity<String> createUser(@RequestBody Users user) {
		String message = userService.createUser(user);
		return ResponseEntity.ok(message);
	}
	
	@PostMapping("/login")
	public ResponseEntity<?> getUser(HttpSession session, @RequestParam("username") String username,
									@RequestParam("password") String password) {
		Users user = userService.checkUserExist(username, password);
		
		if(user != null) {
			session.setAttribute("user", user);
			return ResponseEntity.ok(user.getUser_role());
			
		} else {
			return ResponseEntity.ok(user);
		}
	}
	
	@PostMapping("/logout")
	public ResponseEntity<String> logutUser(HttpSession session) {
		session.invalidate();
		return ResponseEntity.ok("User Logout");
	}
}
