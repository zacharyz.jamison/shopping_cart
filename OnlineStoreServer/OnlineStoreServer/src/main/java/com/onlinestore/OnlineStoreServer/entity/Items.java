package com.onlinestore.OnlineStoreServer.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity(name="items")
public class Items {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="item_id")
	private int item_id;
	@Column(name="item_name")
	private String item_name;
	@Column(name="item_description")
	private String item_description;
	@Column(name="item_price")
	private int item_price;
	@Column(name="item_img_path")
	private String item_img_path;
	
	public Items() {
		super();
	}

	public Items(int item_id, String item_name, String item_description, int item_price, String item_img_path) {
		super();
		this.item_id = item_id;
		this.item_name = item_name;
		this.item_description = item_description;
		this.item_price = item_price;
		this.item_img_path = item_img_path;
	}

	public int getItem_id() {
		return item_id;
	}

	public void setItem_id(int item_id) {
		this.item_id = item_id;
	}

	public String getItem_name() {
		return item_name;
	}

	public void setItem_name(String item_name) {
		this.item_name = item_name;
	}

	public String getItem_description() {
		return item_description;
	}

	public void setItem_description(String item_description) {
		this.item_description = item_description;
	}

	public int getItem_price() {
		return item_price;
	}

	public void setItem_price(int item_price) {
		this.item_price = item_price;
	}

	public String getItem_img_path() {
		return item_img_path;
	}

	public void setItem_img_path(String item_img_path) {
		this.item_img_path = item_img_path;
	}

	@Override
	public String toString() {
		return "Items [item_id=" + item_id + ", item_name=" + item_name + ", item_description=" + item_description
				+ ", item_price=" + item_price + ", item_img_path=" + item_img_path + "]";
	}
	
	
	
	
}
