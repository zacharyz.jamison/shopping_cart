package com.onlinestore.OnlineStoreServer.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity(name="users")
public class Users {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="user_id")
	private int user_id;
	@Column(name="username")
	private String username;
	@Column(name="user_email")
	private String user_email;
	@Column(name="user_password")
	private String password;
	@Column(name="user_role")
	private int user_role;
	
	public Users() {
		super();
	}

	public Users(int user_id, String username, String user_email, String user_password, int user_role) {
		super();
		this.user_id = user_id;
		this.username = username;
		this.user_email = user_email;
		this.password = user_password;
		this.user_role = user_role;
	}

	public int getUser_id() {
		return user_id;
	}

	public void setUser_id(int user_id) {
		this.user_id = user_id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getUser_email() {
		return user_email;
	}

	public void setUser_email(String user_email) {
		this.user_email = user_email;
	}

	public String getUser_password() {
		return password;
	}

	public void setUser_password(String user_password) {
		this.password = user_password;
	}

	public int getUser_role() {
		return user_role;
	}

	public void setUser_role(int user_role) {
		this.user_role = user_role;
	}

	@Override
	public String toString() {
		return "Users [user_id=" + user_id + ", username=" + username + ", user_email=" + user_email
				+ ", user_password=" + password + ", user_role=" + user_role + "]";
	}
	
	

}
