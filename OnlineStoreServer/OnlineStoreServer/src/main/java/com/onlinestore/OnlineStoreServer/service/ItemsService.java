package com.onlinestore.OnlineStoreServer.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.onlinestore.OnlineStoreServer.entity.Items;
import com.onlinestore.OnlineStoreServer.repo.Items_Repository;

@Service
public class ItemsService {
	
	@Autowired
	Items_Repository items_repository;
	
	public List<Items> getAllItems() {
		return items_repository.findAll();
	}
	
	public Items getItems(int item_id) {
		return items_repository.findById(item_id);
	}
	
	public String addItem(Items item) {
		items_repository.save(item);
		
		return "Item inserted";
	}
}
