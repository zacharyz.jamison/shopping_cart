package com.onlinestore.OnlineStoreServer.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.onlinestore.OnlineStoreServer.entity.Items;

public interface Items_Repository extends JpaRepository<Items, Integer>{
	public Items findById(int item_id);
}
