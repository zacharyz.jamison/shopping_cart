package com.onlinestore.OnlineStoreServer.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.onlinestore.OnlineStoreServer.entity.BuyerHistory;
import com.onlinestore.OnlineStoreServer.service.BuyerHistoryService;

@RestController
@RequestMapping("/buyerhistory")
public class BuyerHistoryController {
	@Autowired
	BuyerHistoryService buyerHistoryService;
	
	@GetMapping("/{username}")
	public ResponseEntity<?> getBuyerHistory(@PathVariable("username") String username) {
		List<BuyerHistory> buyerHistory = buyerHistoryService.getHistory(username);
		if(!buyerHistory.isEmpty()) {
			return ResponseEntity.ok(buyerHistory);
		} else {
			return ResponseEntity.ok(new int[0]);
		}
	}
	
	@PostMapping()
	public ResponseEntity<?> insertBuyerHistory(@RequestBody BuyerHistory buyerHistory) {
		return ResponseEntity.ok(buyerHistoryService.insertHistory(buyerHistory));
	}
}
