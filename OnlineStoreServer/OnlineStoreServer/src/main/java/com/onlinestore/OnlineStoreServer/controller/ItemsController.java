package com.onlinestore.OnlineStoreServer.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.onlinestore.OnlineStoreServer.entity.Items;
import com.onlinestore.OnlineStoreServer.service.ItemsService;

@RestController
@RequestMapping("/items")
public class ItemsController {
	
	@Autowired
	ItemsService itemsService;
	
	@GetMapping()
	public ResponseEntity<List<Items>> getItems() {
		return ResponseEntity.ok(itemsService.getAllItems());
	}
	
	@GetMapping("/{item_id}")
	public ResponseEntity<Items> getItem(@PathVariable("item_id") int item_id) {
		return ResponseEntity.ok(itemsService.getItems(item_id));
	}
	
	@PostMapping()
	public ResponseEntity<String> insertItem(@RequestBody Items item) {
		return ResponseEntity.ok(itemsService.addItem(item));
	}
}
