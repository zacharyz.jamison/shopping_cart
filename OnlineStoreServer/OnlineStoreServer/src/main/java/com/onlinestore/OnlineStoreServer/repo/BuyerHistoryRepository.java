package com.onlinestore.OnlineStoreServer.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import com.onlinestore.OnlineStoreServer.entity.BuyerHistory;

public interface BuyerHistoryRepository extends JpaRepository<BuyerHistory, Integer>{
	public List<BuyerHistory> findByBuyerName(String username);
}
