import logo from './logo.svg';
import './App.css';
import MainCmp from './components/MainCmp';
import { BrowserRouter as Router, Route, withRouter } from 'react-router-dom';

function App() {
  return (
    <div className="App">
     
      <MainCmp />
     
    </div>
  );
}

export default App;
