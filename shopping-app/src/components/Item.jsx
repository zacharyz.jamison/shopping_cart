import axios from 'axios';
import { useEffect, useState } from 'react';
import ItemPres from '../presentation-components/ItemPres';
import "../styles/home.css";

export default function Item(props) {

    const [name,setName] = useState(null);
    const [description,setDescription] = useState(null);
    const [price,setPrice] = useState(null);
    const [img,setImg] = useState(null);

    useEffect(() => {
        axios.get("http://localhost:8080/items/"+props.match.params.item).then(res =>{
            setName(res.data.item_name);
            setDescription(res.data.item_description);
            setPrice(res.data.item_price);
            setImg(res.data.item_img_path)
        })
    },[])

    const addToCart = (e) => {
        e.preventDefault();
        let user = localStorage.getItem("username");
        if(user === null || user === "") {
            props.history.replace("/login");
        } else {
            let username = localStorage.getItem("username")
            let items = {username,name,description,price,img}
            let mylist = []
            let shopping = localStorage.getItem("shoppingList");
            if(shopping === null) {
                mylist.push(items);
                localStorage.setItem("shoppingList",JSON.stringify(mylist));
                props.history.push("/home");
            } else {
                mylist = JSON.parse(localStorage.getItem("shoppingList"));
                mylist.push(items);
                localStorage.setItem("shoppingList",JSON.stringify(mylist));
                props.history.push("/home");
            }
          
        }
    }

    if(name===null) {
        return (
            <>
            <h1>Loading Item</h1>
            </>
        )
    }
    else {
        return(
            <div className="main">
                <ItemPres 
                    name={name}
                    description={description}
                    price={price}
                    img={img}
                    setName={setName}
                    setDescription={setDescription}
                    setPrice={setPrice}
                    setImg={setImg}
                    addToCart={addToCart}

                />
            </div>
        )
    }
}