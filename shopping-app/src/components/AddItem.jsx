import { useEffect,useState } from 'react';
import AddItemPres from '../presentation-components/AddItemPres';
import axios from 'axios';
import '../styles/login.css';

export default function AddItem(props) {

    const [userCheck, setUserCheck] = useState(null);
    const [roleCheck, setRoleCheck] = useState("");
    const [itemName, setItemName] = useState("");
    const [itemDes, setItemDes] = useState("");
    const [itemPrice, setItemPrice] = useState(0);


    useEffect(() => {
        setUserCheck(localStorage.getItem("username"))
        let role = JSON.parse(localStorage.getItem("userrole"));
        if(role === null){
            setRoleCheck(0);
        } else {
            setRoleCheck(role.role);
        }
    });

    const handleItem = (e) => {
        e.preventDefault();
        /*
        console.log(itemFile.name);

        let fd = new FormData();
        fd.append('file',itemFile);

        axios.post("",fd).then((res) => {
            setItemUrl(itemFile);
            console.log(itemUrl);
        }) */
        let body = {
            item_name: itemName,
            item_description: itemDes,
            item_price: itemPrice,
            item_img_path: "imgs/holder.svg"
        }
        let user = localStorage.getItem("username");
        let history = {
            buyerName:user,
            name:itemName,
            description:itemDes,
            price:itemPrice
        }

        axios.post("http://localhost:8080/items",body).then(res => {
            axios.post("http://localhost:8080/buyerhistory",history).then(res2=> {
               props.history.push("/home");
            })
        })


    }

    if(roleCheck === 0 || roleCheck === null) {
        //props.history.push("/home");
        return(
            <>
            <h1>Not Allowed</h1>
            </>
        )
    } else {
        return (
            <div className="center">
                <h2>Add Item</h2>
                <br />
                <AddItemPres
                    userCheck={userCheck}
                    setUserCheck={setUserCheck}
                    roleCheck={roleCheck}
                    setRoleCheck={setRoleCheck}
                    itemName={itemName}
                    setItemName={setItemName}
                    itemDes={itemDes}
                    setItemDes={setItemDes}
                    itemPrice={itemPrice}
                    setItemPrice={setItemPrice}
                    handleItem={handleItem}
                
                />
        </div>
        )
    }
}