import { useEffect, useState } from 'react';
import ShoppingCartPres from '../presentation-components/ShoppingCartPres';
import axios from 'axios';
import '../styles/home.css';

export default function ShoppingCart(props) {

    const [cart,checkCart] = useState(null);
    const [total, setTotal] = useState(0);

    useEffect(() => {
        checkCart(JSON.parse(localStorage.getItem("shoppingList")));
        if(cart !== null) {
            console.log(cart[0].price);
            let theTotal = 0;
            for(var i = 0; i < cart.length; i++) {
                //console.log(cart[i].price);
                theTotal += cart[i].price;
            }
            setTotal(theTotal);
        }
    },[total,cart])

    const buyItem = (e) => {
        e.preventDefault();
        axios.post("http://localhost:8080/userhistory",cart).then((res) =>{
            localStorage.clear("shoppingList");
            props.history.push("/home");
        })
    }

    if(cart === null) {
        return (
            <div className="center">
            <h3>Shopping Cart List</h3>
            <br />
            <h2>Sorry nothing in cart</h2>
            </div>
        )
    } else {
        return (
            <div className="center">
            <h3>Shopping Cart List</h3>
            <br />
            <ShoppingCartPres
                cart={cart}
                checkCart={checkCart}
                total={total}
                setTotal={setTotal}
                buyItem={buyItem}
            />
            </div>
        )
    }
}