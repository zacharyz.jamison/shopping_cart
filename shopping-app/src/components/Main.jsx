import { BrowserRouter as Router, Route, useHistory } from 'react-router-dom';
import {Navbar, Nav, Button} from 'react-bootstrap';
import AdminRegistration from './AdminRegistration';
import BuyerRegistration from './BuyerRegistration';
import UserRegistration from './UserRegistration';
import Login from './Login';
import Home from './Home';
import AddItem from './AddItem';
import ViewHistory from './ViewHistory';
import ShoppingCart from './ShoppingCart';
import { useEffect, useState } from 'react';
import axios from 'axios';
import Item from './Item';

export default function Main(props) {

    const [userCheck, setUserCheck] = useState("");

    useEffect(()=> {
         setUserCheck(localStorage.getItem("username"));
         console.log(userCheck);
         console.log("Use Effect Called");
    },[userCheck,localStorage.getItem("username")]);

    const logout = (e) => {
        e.preventDefault();
        localStorage.removeItem("username");
        localStorage.removeItem("password");
        localStorage.removeItem("userrole");
        localStorage.removeItem("shoppingList");
        axios.post("http://localhost:8080/users/logout").then(res =>{
            setUserCheck("");
        })
    }

    if(userCheck === ""  || userCheck === null) {
        return(
            <Router>
                <>
                <Navbar bg="dark" expand="lg">
                    <Navbar.Brand style={{color:'white'}} href="/">Online Store</Navbar.Brand>
                    <Navbar.Toggle aria-controls="basic-navbar-nav" />
                    <Navbar.Collapse id="basic-navbar-nav">
                        <Nav className="mr-auto" style={{color:'white'}}>
                            <Nav.Link style={{color:'white'}} href="/home">Home</Nav.Link>
                            <Nav.Link style={{color:'white'}} href="/login">User Login</Nav.Link>
                            <Nav.Link style={{color:'white'}} href="/register">Register</Nav.Link>
                            <Nav.Link style={{color:'white'}} href="/buyer">Seller Login</Nav.Link>
                        </Nav>
                    </Navbar.Collapse>
                </Navbar>
                <Route exact path="/" component={Home} />
                <Route path="/home" component={Home} />
                <Route path="/admin" component={AdminRegistration} />
                <Route path="/buyer" component={BuyerRegistration} />
                <Route path="/register" component={UserRegistration} />
                <Route path="/login" component={Login} />
                <Route path="/addProducts" component={AddItem} />
                <Route path="/viewHistory" component={ViewHistory} />
                <Route path="/shoppingCart" component={ShoppingCart} />
                <Route path="/item/:item" component={Item} />
                </>
            </Router>
        )}

    else {
        return(
            <Router>
                <>
                <Navbar bg="dark" expand="lg">
                    <Navbar.Brand style={{color:'white'}} href="/">Online Store</Navbar.Brand>
                    <Navbar.Toggle aria-controls="basic-navbar-nav" />
                    <Navbar.Collapse id="basic-navbar-nav">
                        <Nav className="mr-auto" style={{color:'white'}}>
                            <Nav.Link style={{color:'white'}} href="/home">Home</Nav.Link>
                            <Nav.Link style={{color:'white'}} href="/shoppingCart">ShoppingCart</Nav.Link>
                            <Button style={{color:'white'}} onClick={logout}>Logout</Button>
                        </Nav>
                    </Navbar.Collapse>
                </Navbar>
                <Route exact path="/" component={Home} />
                <Route path="/home" component={Home} />
                <Route path="/admin" component={AdminRegistration} />
                <Route path="/buyer" component={BuyerRegistration} />
                <Route path="/register" component={UserRegistration} />
                <Route path="/login" component={Login} />
                <Route path="/addItem" component={AddItem} />
                <Route path="/viewHistory" component={ViewHistory} />
                <Route path="/shoppingCart" component={ShoppingCart} />
                <Route path="/item/:item" component={Item} />
                </>
            </Router>
        )
    }
}

