import axios from 'axios';
import { useState, useEffect } from 'react';
import {Tab, Tabs, Form,Button} from 'react-bootstrap';
import '../styles/login.css';
import ViewHistoryPres from '../presentation-components/ViewHistoryPres';

export default function ViewHistory() {

    const [userCheck, setUserCheck] = useState(null);
    const [roleCheck, setRoleCheck] = useState("");
    const [userSearch, setUserSearch] = useState("");
    const [buyerSearch, setBuyerSearch] = useState("");

    const [userResults,setUserResults] = useState([]);
    const [buyerResults,setBuyerResults] = useState([]);

    useEffect(() => {
        setUserCheck(localStorage.getItem("username"))
        let role = JSON.parse(localStorage.getItem("userrole"));
        if(role === null){
            setRoleCheck(0);
        } else {
            setRoleCheck(role.user_role);
        }
    },[]);

    const handleUserHistory = (e) => {
        e.preventDefault();
        axios.get("http://localhost:8080/userhistory/"+userSearch).then(res=>{
            setUserResults(res.data);
            console.log(res.data);
        })
    }

    const handleBuyerHistory = (e) => {
        e.preventDefault();
        axios.get("http://localhost:8080/buyerhistory/"+buyerSearch).then(res=>{
            setBuyerResults(res.data);
            console.log(res.data);
        })
    }

    if(roleCheck === 0 || roleCheck === null || roleCheck === 1) {
        //props.history.push("/home");
        return(
            <>
            <h1>Not Allowed</h1>
            </>
        )
    } else {
        return (
            <>
                <Tabs defaultActiveKey="profile" id="uncontrolled-tab-example">
                    <Tab eventKey="buyer" title="User History">
                    <div className="center">
                        <Form>
                            <Form.Group controlId="formSearhUser">
                                <Form.Label>User:</Form.Label>
                                <Form.Control 
                                    type="text" 
                                    placeholder="Enter Username"
                                    value={userSearch}
                                    onChange={(e) => {setUserSearch(e.target.value)}}/>
                            </Form.Group>
                            <Button variant="primary" type="submit" onClick={handleUserHistory}>
                                Search
                            </Button>
                        </Form>
                        <br />
                        <ViewHistoryPres userResults={userResults} />
                    </div>
                    </Tab>
                    <Tab eventKey="seller" title="Seller History">
                        <div className="center">
                            <Form>
                                <Form.Group controlId="formSearhBuyer">
                                    <Form.Label>Buyer Name:</Form.Label>
                                    <Form.Control 
                                        type="text" 
                                        placeholder="Enter Buyer Name"
                                        value={buyerSearch}
                                        onChange={(e) => {setBuyerSearch(e.target.value)}}/>
                                </Form.Group>
                                <Button variant="primary" type="submit" onClick={handleBuyerHistory}>
                                    Search
                                </Button>
                            </Form>
                            <br />
                             <ViewHistoryPres userResults={buyerResults} />
                        </div>
                    </Tab>
                </Tabs>
            </>
        )
    }
}