import axios from 'axios';
import { useState } from 'react';
import LoginPres from '../presentation-components/LoginPres';

import '../styles/login.css';

export default function Login(props) {

    const [username,setUsername] = useState("");
    const [password,setPassword] = useState("");
    const [message, setMessage] = useState("");
    
    const onhandleSubmit = (e) => {
        e.preventDefault();

        if(username !== "" && password !== "") {
            axios.post("http://localhost:8080/users/login?username="+username
            +"&password="+password).then(res =>{
                console.log(res.data);
                if(res.data === "") {
                    setMessage("Invalid username/password");
                } else {
                    let role = {user_role: res.data}
                    localStorage.setItem("username",username)
                    localStorage.setItem("password",password)
                    localStorage.setItem("userrole",JSON.stringify(role));
                    props.history.push("/home");
            }
            })
        } else {
            setMessage("All Fields Required!!!");
        }
    }

    return (
        <div className="center">
            <h2>Login</h2>
            <br />
            <LoginPres
                username={username}
                password={password}
                message={message}
                setUsername={setUsername}
                setPassword={setPassword}
                setMessage={setMessage}
                onhandleSubmit={onhandleSubmit}
            />
        </div>
    )
}