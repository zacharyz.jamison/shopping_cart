import axios from 'axios';
import { useEffect, useState } from 'react';
import {Jumbotron, Container, Card, Button, Row} from 'react-bootstrap';
import '../styles/home.css';

export default function Home(props) {
    
    const [items,setItems] = useState(null);

    useEffect(() => {
        axios.get("http://localhost:8080/items").then((res) => {
                setItems(res.data);
                console.log(res.data);
            })
    },[])

    if(items === null) {
        return (
            <h2>Loading items</h2>
        );
    } else {
        return (
            <>
                <Jumbotron fluid>
                    <Container>
                        <h1>Welcome To Online Store</h1>
                        <p>
                        Come and buy the latest products today
                        </p>
                    </Container>
                </Jumbotron>
                <br />
                <div className="main">
                    <Container>
                        <Row>
                            {items.map(item => (
                                <div key={item.item_id}>
                                <Card style={{ width: '18rem', marginRight:'1rem', marginBottom:'1rem'}}>
                                    <Card.Img variant="top" src={process.env.PUBLIC_URL+item.item_img_path} />
                                    <Card.Body>
                                        <Card.Title>{item.item_name}</Card.Title>
                                        <Card.Text>
                                           Description: {item.item_description}
                                        </Card.Text>
                                        <Card.Text>
                                           Price: ${item.item_price}
                                        </Card.Text>
                                        <Button variant="primary" onClick={() => {props.history.push("/item/"+item.item_id)}}>
                                            Click On Item</Button>
                                    </Card.Body>
                                </Card>
                                </div>
                            ))}
                        </Row>
                    </Container>
                </div>
            </>
        )
    }
}