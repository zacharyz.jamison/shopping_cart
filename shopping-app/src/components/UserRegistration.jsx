import axios from 'axios';
import { useState } from 'react';
import '../styles/login.css';
import RegistrationPres from '../presentation-components/RegistrationPres';

export default function UserRegistration(props) {

    const [username,setUsername] = useState("");
    const [password,setPassword] = useState("");
    const [email,setEmail] = useState("");
    const [role,setRole] = useState(0);
    const [message, setMessage] = useState("");

    const handleSubmit = (e) => {
        e.preventDefault();
        if(username !== "" && password !== "" && email !== "") {
            setRole(0);
            let user = {
                username:username,
                user_email:email,
                user_role:role,
                user_password:password,
            }
            axios.post("http://localhost:8080/users",user).then((res) =>{
                props.history.push("/home");
            })
        } else {
            setMessage("All fields Required!!!");
        }
    }

    return (
        <div className="center">
            <h2>Registration</h2>
            <br />
            
            <RegistrationPres 
                handleSubmit={handleSubmit}
                username={username}
                password={password}
                email={email}
                message={message}
                setUsername={setUsername}
                setPassword={setPassword}
                setEmail={setEmail}
                setMessage={setMessage}
                />
        </div>
    )
}