import { useState } from 'react';
import axios from 'axios';
import '../styles/login.css';
import RegistrationPres from '../presentation-components/RegistrationPres';

export default function AdminRegistration(props) {

    const [username,setUsername] = useState("");
    const [password,setPassword] = useState("");
    const [email,setEmail] = useState("");
    const [role,setRole] = useState(0);
    const [message, setMessage] = useState("");

    const handleSubmit = (e) => {
        e.preventDefault();
        if(username !== "" && password !== "" && email !== "") {
            setRole(2);
            let user = {
                username:username,
                user_email:email,
                user_role:role,
                user_password:password,
            }
            axios.post("http://localhost:8080/users",user).then((res) =>{
                props.history.push("/home");
            })
        } else {
            setMessage("All Fields Required!!!");
        }
    }

    return (
        <div className="center">
            <h2>Admin Registration</h2>
            <br />
            <RegistrationPres 
                handleSubmit={handleSubmit}
                username={username}
                password={password}
                email={email}
                message={message}
                setUsername={setUsername}
                setPassword={setPassword}
                setEmail={setEmail}
                setMessage={setMessage}
                />
        </div>
    )
}