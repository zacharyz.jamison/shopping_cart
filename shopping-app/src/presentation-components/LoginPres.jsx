import {Form,Button} from 'react-bootstrap';
export default function LoginPres(props) {
    return (
        <>
            <h4 style={{color:'red'}}>{props.message}</h4>
            <Form>
                <Form.Group controlId="formBasicEmail">
                    <Form.Label>Username</Form.Label>
                    <Form.Control type="text" value={props.username} 
                        onChange={(e)=>{props.setUsername(e.target.value)}} 
                        placeholder="Enter username" />
                </Form.Group>

                <Form.Group controlId="formBasicPassword">
                    <Form.Label>Password</Form.Label>
                    <Form.Control type="password" value={props.password}
                        onChange={(e)=>{props.setPassword(e.target.value)}}
                        placeholder="Password" />
                </Form.Group>

                <Button variant="primary" type="submit" onClick={props.onhandleSubmit}>
                    Submit
                </Button>
            </Form>
        </>
    )
}