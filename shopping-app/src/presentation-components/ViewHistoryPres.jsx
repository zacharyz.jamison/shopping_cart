import {ListGroup} from 'react-bootstrap';

export default function ViewHistoryPres(props) {

    return (
        <>
            {props.userResults.map(item => (
                <div key={item.id}>
                        <ListGroup>
                        <ListGroup.Item>Name: {item.name}</ListGroup.Item>
                        <ListGroup.Item>Description: {item.description}</ListGroup.Item>
                        <ListGroup.Item>Price: ${item.price}</ListGroup.Item>
                        </ListGroup>
                        <br />
                </div>
            ))}
        </>
    )
}