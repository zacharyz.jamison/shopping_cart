import {Form,Button} from 'react-bootstrap';

export default function RegistrationPres(props) {
    return (
        <>
            <h4 style={{color:'red'}}>{props.message}</h4>
            <Form>
                <Form.Group controlId="formBasicEmail">
                    <Form.Label>Username</Form.Label>
                    <Form.Control 
                        type="text" 
                        placeholder="Enter username"
                        value={props.username} 
                        onChange={(e) => {props.setUsername(e.target.value)}}/>
                </Form.Group>

                <Form.Group controlId="formBasicEmail">
                    <Form.Label>Email</Form.Label>
                    <Form.Control 
                        type="email" 
                        placeholder="Enter Email" 
                        value={props.email} 
                        onChange={(e) => {props.setEmail(e.target.value)}}/>
                </Form.Group>

                <Form.Group controlId="formBasicPassword">
                    <Form.Label>Password</Form.Label>
                    <Form.Control 
                        type="password" 
                        placeholder="Password" 
                        value={props.password} 
                        onChange={(e) => {props.setPassword(e.target.value)}}/>
                </Form.Group>

                <Button variant="primary" type="submit" onClick={props.handleSubmit}>
                    Submit
                </Button>
            </Form>
        </>
    );
}