import {Container, Card, Button, Row, ListGroup} from 'react-bootstrap';

export default function ShoppingCartPres(props) {
    return(
        <>
            <Container>
                <Row>
                    {props.cart.map(item => (
                        <div key={item.name}>
                        <Card style={{ width: '10rem', marginRight:'1rem', marginBottom:'1rem'}}>
                            <Card.Img variant="top" src={process.env.PUBLIC_URL+"/"+item.img} />
                            <Card.Body>
                                <Card.Title>{item.name}</Card.Title>
                                <Card.Text>
                                    Description: {item.description}
                                </Card.Text>
                                <Card.Text>
                                    Price: ${item.price}
                                </Card.Text>
                            </Card.Body>
                        </Card>
                        </div>
                    ))}
                    <ListGroup>
                            <ListGroup.Item>Total: ${props.total}</ListGroup.Item>
                        </ListGroup>
                </Row>

                <Button variant="primary" onClick={props.buyItem}>
                                            Buy Items</Button>
            </Container>
        </>
    )
}