import {ListGroup, Button, Card} from 'react-bootstrap';

export default function ItemPres(props) {
    return(
    <>
        <br />
                <ListGroup>
                    <ListGroup.Item>Name: {props.name}</ListGroup.Item>
                    <ListGroup.Item>Description: {props.description}</ListGroup.Item>
                    <ListGroup.Item>Price: ${props.price}</ListGroup.Item>
                    <ListGroup.Item>
                    <Card.Img variant="top" src={process.env.PUBLIC_URL+"/"+props.img} />
                    </ListGroup.Item>
                </ListGroup>
                <br />
                <Button variant="primary" onClick={(props.addToCart)}>
                                                Add To Cart</Button>
    </>
    )
}