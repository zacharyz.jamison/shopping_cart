import {Form,Button} from 'react-bootstrap';

export default function AddItemPres(props) {
    return(
        <>
            <Form>
                <Form.Group controlId="formItemName">
                    <Form.Label>Item Name</Form.Label>
                    <Form.Control 
                        type="text" 
                        placeholder="Enter Item Name"
                        value={props.itemName}
                        onChange={(e) => {props.setItemName(e.target.value)}}/>
                </Form.Group>

                <Form.Group controlId="formItemDescription">
                    <Form.Label>Description</Form.Label>
                    <Form.Control 
                        type="text" 
                        placeholder="Description" 
                        value={props.itemDes}
                        onChange={(e) => {props.setItemDes(e.target.value)}}/>
                </Form.Group>

                <Form.Group controlId="formItemPrice">
                    <Form.Label>Price</Form.Label>
                    <Form.Control 
                        type="text" 
                        placeholder="Price"
                        value={props.itemPrice}
                        onChange={(e) => {props.setItemPrice(e.target.value)}} />
                </Form.Group>

                <Button variant="primary" type="submit" onClick={props.handleItem}>
                    Add Item
                </Button>
            </Form>
        </>
    )
}